#!/bin/bash -xe

device="$1"
output="$2"
ver="$3"

dir="out/target/product/$device/images"

cd "/opt/halium/$ver"

echo "Working on device: $device"

if [ ! -f "$dir/system.img" ]; then
    echo "system.img does not exist"
    exit 1
fi

workdir=$(mktemp -d /tmp/halium.XXXXXXXX)
mkdir "$workdir/partitions"

mkdir -p "$workdir/system/var/lib/lxc/android/"

fileType=$(file -b0 "$dir/system.img")
if [[ $fileType == "Android sparse image"* ]]; then
    echo "Converting sparse image to image"
    mv "$dir/system.img" "$dir/system.sparse.img"
    simg2img "$dir/system.sparse.img" "$dir/system.img"
    e2fsck -fy "$dir/system.img" || true
    resize2fs -p -M "$dir/system.img"
fi

cp "$dir/system.img" "$workdir/system/var/lib/lxc/android/android-rootfs.img"

tar -cf "$output/halium_${device}.tar" -C "$workdir" \
    --owner=root --group=root \
    partitions/ system/
xz --threads=0 -1 "$output/halium_${device}.tar"

rm -rf ./out/
rm -r "$workdir"
