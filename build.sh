#!/bin/bash -xe

device="$1"
lunch="$2"
ver="$3"

# sync

cd "/opt/halium/$ver"
rm -f ./.repo/local_manifests/*

repo sync -c --force-sync --force-remove-dirty
./halium/devices/setup "$device" --force-sync --force-remove-dirty

# build

rm -rf ./out/

if [ -d "hybris-patches" ]; then
    hybris-patches/apply-patches.sh --mb
fi

export LC_ALL=C
export USE_CCACHE=1
source build/envsetup.sh
lunch "$lunch-userdebug"

time make -j10 systemimage

mkdir "./out/target/product/$device/images"
mv ./out/target/product/"$device"/*.img "./out/target/product/$device/images/"
