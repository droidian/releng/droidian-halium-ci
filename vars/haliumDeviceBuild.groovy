def call(String haliumVersion, String deviceName, String lunchName) {
  String agentTag = "halium-$haliumVersion"
  String artifacts = 'halium*'

  pipeline {
    agent { label "$agentTag" }
    options {
      skipDefaultCheckout()
      buildDiscarder(logRotator(artifactNumToKeepStr: '30'))
      throttle(['halium'])
    }
    triggers {
      cron('@weekly')
    }
    stages {
      stage('Clean before building') {
        steps {
          deleteDir()
        }
      }
      stage('Build port') {
        steps {
          checkout scm
          dir ('lastArtifacts') {
            script {
              copyArtifacts(projectName: "${JOB_NAME}",
                            selector: lastSuccessful(),
                            optional: true)
            }
          }
          dir ('scripts') {
            git(branch: 'droidian',
                url: 'https://github.com/droidian-releng/droidian-halium-ci')
            sh """
              nice ./build.sh '$deviceName' '$lunchName' '$haliumVersion'
              nice ./build-tarball.sh '$deviceName' '$WORKSPACE' '$haliumVersion'
            """
          }
        }
      }
    }
    post {
      cleanup {
        deleteDir()
      }
    }
  }
}
